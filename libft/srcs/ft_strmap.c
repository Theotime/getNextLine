/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 17:27:12 by triviere          #+#    #+#             */
/*   Updated: 2013/11/27 17:30:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		len;
	int		i;
	char	*str;

	len = ft_strlen(s);
	str = ft_memalloc(len + 1);
	i = 0;
	while (*s)
	{
		str[i] = f(*s);
		i++;
		s++;
	}
	return (str);
}
