/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 04:15:11 by triviere          #+#    #+#             */
/*   Updated: 2015/11/26 14:29:13 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static	int		ft_itoa_strlen(unsigned int nbr)
{
	int	i;

	i = 0;
	while (nbr && (++i))
		nbr /= 10;
	return (i);
}

void			ft_itoa_2(unsigned int nbr, char *buffer)
{
	char	c;

	if (nbr)
	{
		c = nbr % 10 + 48;
		ft_itoa_2(nbr / 10, buffer - 1);
		*buffer = c;
	}
}

char			*ft_itoa(int nbr)
{
	int					length;
	int					neg;
	int					i;
	char				*result;

	if (nbr == 0)
		return (ft_strdup("0"));
	neg = 0;
	i = 0;
	length = ft_itoa_strlen((nbr < 0) ? nbr * -1 : nbr);
	if (nbr < 0)
	{
		result = malloc(sizeof(char) * (length + 2));
		result[0] = '-';
		result[length + 1] = 0;
		ft_itoa_2(nbr * -1, result + length);
	}
	else
	{
		result = malloc(sizeof(char) * (length + 1));
		result[length] = 0;
		ft_itoa_2(nbr, result + length - 1);
	}
	return (result);
}
