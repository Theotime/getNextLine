/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 17:59:14 by triviere          #+#    #+#             */
/*   Updated: 2013/12/02 13:36:07 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strtrim(char const *s)
{
	char	*str;
	int		i;
	int		end;
	int		j;

	i = 0;
	end = ft_strlen(s) - 1;
	while ((s[i] == '\t' || s[i] == '\n' || s[i] == ' ') && i < end)
		i++;
	while ((s[end] == '\t' || s[end] == '\n' || s[end] == ' ') && end >= 0)
		end--;
	if (end >= 0)
	{
		str = ft_strnew(end - i + 1);
		j = 0;
		while (i <= end)
		{
			str[j] = s[i];
			i++;
			j++;
		}
	}
	else
		str = ft_strnew(0);
	return (str);
}
