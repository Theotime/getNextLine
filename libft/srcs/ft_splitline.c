/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_splitline.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 07:22:31 by triviere          #+#    #+#             */
/*   Updated: 2015/12/15 09:23:25 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_splitline(char const *s, char ***line)
{
	int		i;
	int		j;
	int		k;
	char	**result;

	i = -1;
	j = 0;
	k = 0;
	result = *line;
	result = (char**)malloc(sizeof(char*) * ft_strcount(s, '\n') + 1);
	while (s[++i] != '\0')
	{
		if (s[i] == '\n' || !s[i + 1])
		{
			result[j++] = ft_strsub(s, (i - k), k);
			k = 0;
		}
		else
			k++;
	}
	if (j == 0)
		result[j++] = ft_strdup(s);
	*result[j] = '\0';
	*line = result;
	return (j);
}
