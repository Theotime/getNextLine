/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 11:11:23 by triviere          #+#    #+#             */
/*   Updated: 2013/11/25 11:25:31 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strstr(char const *s1, char const *s2)
{
	int		i;
	int		length2;

	length2 = ft_strlen(s2);
	if (!s2)
		return ((char *)s1);
	i = 0;
	while (*s1)
	{
		if (((char *)s2)[i] == '\0')
			return ((char *)s1 - i);
		else if (*s1 == s2[i])
			i++;
		else if (i > 0)
		{
			s1 -= i;
			i = 0;
		}
		s1++;
	}
	return ((*s1 == s2[i] && !s2[i]) ? (char *)(s1 - i) : 0);
}
