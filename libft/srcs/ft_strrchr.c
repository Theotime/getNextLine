/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 11:00:12 by triviere          #+#    #+#             */
/*   Updated: 2013/11/29 12:42:00 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strrchr(char const *s, int c)
{
	int		length;

	length = ft_strlen(s);
	s += length;
	while (length--)
	{
		if ((char)*s == (char)c)
			return ((char *)s);
		s--;
	}
	if ((char)*s == (char)c)
		return ((char *)s);
	return (NULL);
}
