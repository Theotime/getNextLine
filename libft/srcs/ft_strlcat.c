/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:44:15 by triviere          #+#    #+#             */
/*   Updated: 2013/11/25 17:53:20 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

size_t		ft_strlcat(char *dest, char const *src, size_t dest_size)
{
	char		*dest2;
	const char	*src2;
	size_t		dest_len;

	dest2 = dest;
	src2 = src;
	dest_len = dest_size + 1;
	while (*dest2 && (dest_len -= 1))
		++dest2;
	dest_len = dest2 - dest;
	dest_size = dest_size - dest_len;
	if (dest_size == 0)
		return (dest_len + ft_strlen(src));
	while (*src2)
	{
		if (dest_size != 1)
		{
			*dest2 = *src2;
			dest2++;
			--dest_size;
		}
		++src2;
	}
	*dest2 = 0;
	return (dest_len + (src2 - src));
}
