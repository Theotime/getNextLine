/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 03:07:24 by triviere          #+#    #+#             */
/*   Updated: 2015/12/23 13:04:30 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "libft/includes/libft.h"
# include <fcntl.h>

# define BUFF_SIZE 80

typedef struct		s_fd
{
	int				fd;
	int				ret;
	int				i;
	int				line;
	char			*old;
	char			*buff;
	struct s_fd		*next;
}					t_fd;

int					get_next_line(int const fd, char **line);

#endif
